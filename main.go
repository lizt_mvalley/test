package main

import (
	"database/sql"
	"github.com/jinzhu/gorm"
	"log"
	"strconv"
	"strings"
	"unsafe"
	_ "unsafe"
)

// 思路为：将 SqlExpr 数据指针重新给定一个数据类型
// 用于接收 SqlExpr 的真实数据
type MockSql struct {
	expr string
	args []interface{}
}

// 用于解除底层 sql 的依赖
type MockDB struct{}

func (*MockDB) Exec(query string, args ...interface{}) (sql.Result, error) { return nil, nil }
func (*MockDB) Prepare(query string) (*sql.Stmt, error)                    { return nil, nil }
func (*MockDB) Query(query string, args ...interface{}) (*sql.Rows, error) { return nil, nil }
func (*MockDB) QueryRow(query string, args ...interface{}) *sql.Row        { return nil }

func main() {
	db, err := gorm.Open("mysql", &MockDB{})
	if err != nil {
		log.Fatalln(err)
	}

	subQuery := db.Table("person_jobs").Select("person_id").
		Where("work_at_org_id = ? AND end_on IS NULL", 1)

	sqlBuilder := db.Model("models").Select("person_jobs.*").
		Joins(`LEFT JOIN person_jobs pj2 ON person_jobs.person_id = pj2.person_id AND person_jobs.end_on < pj2.end_on AND person_jobs.work_at_org_id = ? AND pj2.work_at_org_id = ?`, 2, 3).
		Where("person_jobs.work_at_org_id = ?", 4).
		Where("person_jobs.end_on IS NOT NULL").
		// rule 2
		Where("pj2.person_id IS NULL").
		// rule 1
		Where("person_jobs.person_id NOT IN ?", subQuery.QueryExpr())

	log.Println("subQuery:", subQuery.QueryExpr())
	SqlToString(subQuery)
	log.Println("sqlBuilder:", sqlBuilder.QueryExpr())
	SqlToString(sqlBuilder)
}

func SqlToString(db *gorm.DB) string {
	pointer := unsafe.Pointer(db.QueryExpr())
	mockSql := (*MockSql)(pointer)
	log.Println("mock sql args:", mockSql.args)
	for _, arg := range mockSql.args {
		switch argType := arg.(type) {
		case int:
			log.Println("got an int", arg)
			mockSql.expr = strings.Replace(mockSql.expr, "?", strconv.Itoa(arg.(int)), 1)
			log.Println(mockSql.expr)
		case string:
			mockSql.expr = strings.Replace(mockSql.expr, "?", arg.(string), 1)
		case *gorm.DB:
			SqlToString(arg.(*gorm.DB))
		default:
			log.Fatalln("type error:", argType)
		}
	}

	log.Println("mock sql expr:", mockSql.expr)
	return mockSql.expr
}
